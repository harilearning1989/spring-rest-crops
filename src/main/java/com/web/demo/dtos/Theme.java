package com.web.demo.dtos;

public class Theme {

    private String defaultFolder;

    public String getDefaultFolder() {
        return defaultFolder;
    }

    public void setDefaultFolder(String defaultFolder) {
        this.defaultFolder = defaultFolder;
    }
}
